export function settings(model = '') {
    return {
        templateName: 'settings',
        templateValue: `
			<h3>${model.title}</h3>
			`
    };
}