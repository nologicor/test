/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	var _init = __webpack_require__(1);

	window.onload = function () {
	    var init = new _init.Init();
	    window.init = init;
	    init.loadTemplate();
	};

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.Init = undefined;

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _aboutTemplate = __webpack_require__(2);

	var _settingsTemplate = __webpack_require__(3);

	var _option1Template = __webpack_require__(4);

	var _option2Template = __webpack_require__(5);

	var _option3Template = __webpack_require__(6);

	var _aboutController = __webpack_require__(7);

	var _aboutModel = __webpack_require__(8);

	var _settingsModel = __webpack_require__(10);

	var _option1Model = __webpack_require__(11);

	var _option2Model = __webpack_require__(12);

	var _option3Model = __webpack_require__(13);

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var aboutModel = new _aboutModel.AboutModel('About', 'Jessica Parker', 'www.seller.com', '(949) 325 - 68594', 'Newport Beach, CA');
	var settingsModel = new _settingsModel.SettingsModel('Settings');
	var option1Model = new _option1Model.Option1Model('Option1');
	var option2Model = new _option2Model.Option2Model('Option2');
	var option3Model = new _option3Model.Option3Model('Option3');
	var templateContentId = 'template-content';

	var Init = exports.Init = function () {
	    function Init() {
	        _classCallCheck(this, Init);

	        this.aboutModel = aboutModel;
	        this.settingsModel = settingsModel;
	        this.option1Model = option1Model;
	        this.option2Model = option2Model;
	        this.option3Model = option3Model;

	        this.templateList = this.tpls();
	    }

	    _createClass(Init, [{
	        key: 'tpls',
	        value: function tpls() {
	            return [(0, _aboutTemplate.about)(this.aboutModel), (0, _settingsTemplate.settings)(this.settingsModel), (0, _option1Template.option1)(this.option1Model), (0, _option2Template.option2)(this.option2Model), (0, _option3Template.option3)(this.option3Model)];
	        }
	    }, {
	        key: 'updateTpls',
	        value: function updateTpls() {
	            this.templateList = this.tpls();
	        }
	    }, {
	        key: 'getNabarActiveLink',
	        value: function getNabarActiveLink() {
	            var elements = document.getElementsByClassName('page-link');

	            for (var i = 0; i < elements.length; i++) {
	                var element = elements[i];
	                //console.log(element)
	                if (element.parentElement.classList.contains('nav-ul__li--active')) {
	                    return element.getAttribute('href').substring(1);
	                }
	            }
	            throw new Error('No active link');
	        }
	    }, {
	        key: 'loadTemplate',
	        value: function loadTemplate() {
	            var activeLink = this.getNabarActiveLink();
	            for (var i = 0; i < this.templateList.length; i++) {
	                var t = this.templateList[i];
	                if (t.templateName == activeLink) {
	                    var element = document.getElementById(templateContentId);
	                    element.innerHTML = t.templateValue;
	                    this.aboutModel.bind(this.aboutModel, 'body');
	                    return;
	                }
	            }

	            throw new Error('Link and template do not match');
	        }
	    }, {
	        key: 'activateLink',
	        value: function activateLink(obj) {
	            var elements = document.getElementsByClassName('nav-ul__li--active');
	            if (elements.length == 0) throw new Error('No active link');
	            var element = elements[0];
	            element.classList.remove('nav-ul__li--active');
	            obj.parentElement.classList.add('nav-ul__li--active');
	            this.loadTemplate();
	        }
	    }, {
	        key: 'san',
	        value: function san(data) {
	            this.aboutModel = (0, _aboutController.saveAboutName)(data, this.aboutModel);
	            this.updateTpls();
	        }
	    }, {
	        key: 'sas',
	        value: function sas(data) {
	            this.aboutModel = (0, _aboutController.saveAboutSite)(data, this.aboutModel);
	            this.updateTpls();
	        }
	    }, {
	        key: 'sap',
	        value: function sap(data) {
	            this.aboutModel = (0, _aboutController.saveAboutPhone)(data, this.aboutModel);
	            this.updateTpls();
	        }
	    }, {
	        key: 'sal',
	        value: function sal(data) {
	            this.aboutModel = (0, _aboutController.saveAboutLocation)(data, this.aboutModel);
	            this.updateTpls();
	        }
	    }, {
	        key: 'oen',
	        value: function oen(data) {
	            (0, _aboutController.openEditName)(data);
	        }
	    }, {
	        key: 'oes',
	        value: function oes(data) {
	            (0, _aboutController.openEditSite)(data);
	        }
	    }, {
	        key: 'oep',
	        value: function oep(data) {
	            (0, _aboutController.openEditPhone)(data);
	        }
	    }, {
	        key: 'oel',
	        value: function oel(data) {
	            (0, _aboutController.openEditLocation)(data);
	        }
	    }, {
	        key: 'cancel',
	        value: function cancel(data) {
	            return (0, _aboutController.cancel)(data);
	            this.updateTpls();
	        }
	    }]);

	    return Init;
	}();

/***/ }),
/* 2 */
/***/ (function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	exports.about = about;
	function about() {
		var model = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

		var inputName = 'input-name',
		    inputSite = 'input-site',
		    inputPhone = 'input-phone',
		    inputLocation = 'input-location';

		return {
			templateName: 'about',
			templateValue: '\n\t\t\t<h3>' + model.title + '</h3>\n\t\t\t<br>\n\t\t\t<div class="about">\n\t\t\t\t<div class="about__field about__field--first">\n\t\t\t\t\t<div class="about__field__info">\n\t\t\t\t\t\t<span class="about__field__info__data" data-value="name"></span>\n\t\t\t\t\t\t<span onclick="init.oen()" class="about__field__info__edit"><i class="ion-android-create"></i></span>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div id="toggle-name" class="about__field__inputs hide">\n\t\t\t\t\t\t<div class="input-group">\n\t\t\t\t\t\t\t<input type="text" id="' + inputName + '" required>\n\t\t\t\t\t\t\t<span class="highlight"></span>\n\t\t\t\t\t\t\t<span class="bar"></span>\n\t\t\t\t\t\t\t<label>NAME</label>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class="class="about__field__inputs--button">\n\t\t\t\t\t\t\t<input class="button--full" type="submit" value="Submit" onclick="init.san(\'' + inputName + '\'); return false;">\n\t\t\t\t\t\t\t<input class="button--empty" type="submit" value="Cancel" onclick="init.cancel(\'' + inputName + '\'); return false;">\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<br>\n\t\t\t\t<div class="about__field">\n\t\t\t\t\t<div class="about__field__info">\n\t\t\t\t\t\t<i class="ion-android-globe about__field__icon"></i>\n\t\t\t\t\t\t<span class="about__field__info__data" data-value="site">' + model.site + '</span>\n\t\t\t\t\t\t<span onclick="init.oes()" class="about__field__info__edit"><i class="ion-android-create about__field__edit"></i></span>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div id="toggle-site" class="about__field__inputs hide">\n\t\t\t\t\t\t<div class="input-group">\n\t\t\t\t\t\t\t<input type="text" id="' + inputSite + '" required>\n\t\t\t\t\t\t\t<span class="highlight"></span>\n\t\t\t\t\t\t\t<span class="bar"></span>\n\t\t\t\t\t\t\t<label>WEBSITE</label>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class="about__field__inputs--button">\n\t\t\t\t\t\t\t<input class="button--full" type="submit" value="Submit" onclick="init.sas(\'' + inputSite + '\'); return false;">\n\t\t\t\t\t\t\t<input class="button--empty" type="submit" value="Cancel" onclick="init.cancel(\'' + inputSite + '\'); return false;">\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t\n\t\t\t\t</div>\n\t\t\t\t<br>\n\n\t\t\t\t<div class="about__field">\n\t\t\t\t\t<div class="about__field__info">\n\t\t\t\t\t\t<i class="ion-ios-telephone-outline about__field__icon"></i>\n\t\t\t\t\t\t<span class="about__field__info__data" data-value="phone">' + model.phone + '</span>\n\t\t\t\t\t\t<span onclick="init.oep()" class="about__field__info__edit"><i class="ion-android-create about__field__edit"></i></span>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div id="toggle-phone" class="about__field__inputs hide">\n\t\t\t\t\t\t<div class="input-group">\n\t\t\t\t\t\t\t<input type="text" id="' + inputPhone + '" required>\n\t\t\t\t\t\t\t<span class="highlight"></span>\n\t\t\t\t\t\t\t<span class="bar"></span>\n\t\t\t\t\t\t\t<label>Phone</label>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class="about__field__inputs--button">\n\t\t\t\t\t\t\t<input class="button--full" type="submit" value="Submit" onclick="init.sap(\'' + inputPhone + '\'); return false;">\n\t\t\t\t\t\t\t<input class="button--empty" type="submit" value="Cancel" onclick="init.cancel(\'' + inputPhone + '\'); return false;">\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<br>\n\t\t\t\t<div class="about__field">\n\t\t\t\t\t<div class="about__field__info">\n\t\t\t\t\t<i class="ion-ios-home-outline about__field__icon"></i>\n\t\t\t\t\t\t<span class="about__field__info__data" data-value="location">' + model.location + '</span>\n\t\t\t\t\t\t<span onclick="init.oel()" class="about__field__info__edit"><i class="ion-android-create about__field__edit"></i></span>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div id="toggle-location" class="about__field__inputs hide">\n\t\t\t\t\t\t<div class="input-group">\n\t\t\t\t\t\t\t<input type="text" id="' + inputLocation + '" required>\n\t\t\t\t\t\t\t<span class="highlight"></span>\n\t\t\t\t\t\t\t<span class="bar"></span>\n\t\t\t\t\t\t\t<label>CITY,STATE & ZIP</label>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class="about__field__inputs--button">\n\t\t\t\t\t\t\t<input class="button--full" type="submit" value="Submit" onclick="init.sal(\'' + inputLocation + '\'); return false;">\n\t\t\t\t\t\t\t<input class="button--empty" type="submit" value="Cancel" onclick="init.cancel(\'' + inputLocation + '\'); return false;">\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t'
		};
	}

/***/ }),
/* 3 */
/***/ (function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.settings = settings;
	function settings() {
	    var model = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

	    return {
	        templateName: 'settings',
	        templateValue: '\n\t\t\t<h3>' + model.title + '</h3>\n\t\t\t'
	    };
	}

/***/ }),
/* 4 */
/***/ (function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.option1 = option1;
	function option1() {
	    var model = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

	    return {
	        templateName: 'option1',
	        templateValue: '\n\t\t\t<h3>' + model.title + '</h3>\n\t\t\t'
	    };
	}

/***/ }),
/* 5 */
/***/ (function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.option2 = option2;
	function option2() {
	    var model = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

	    return {
	        templateName: 'option2',
	        templateValue: '\n\t\t\t<h3>' + model.title + '</h3>\n\t\t\t'
	    };
	}

/***/ }),
/* 6 */
/***/ (function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.option3 = option3;
	function option3() {
	    var model = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

	    return {
	        templateName: 'option3',
	        templateValue: '\n\t\t\t<h3>' + model.title + '</h3>\n\t\t\t'
	    };
	}

/***/ }),
/* 7 */
/***/ (function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.saveAboutName = saveAboutName;
	exports.saveAboutSite = saveAboutSite;
	exports.saveAboutPhone = saveAboutPhone;
	exports.saveAboutLocation = saveAboutLocation;
	exports.cancel = cancel;
	exports.openEditName = openEditName;
	exports.openEditSite = openEditSite;
	exports.openEditPhone = openEditPhone;
	exports.openEditLocation = openEditLocation;
	function saveAboutName(elId, model) {
	    var input = document.getElementById(elId);
	    var hide = document.getElementById('toggle-name');
	    hide.classList.add('hide');
	    model.name = input.value;
	    input.value = '';
	    model.bind(model, 'body');
	    return model;
	}

	function saveAboutSite(elId, model) {
	    var input = document.getElementById(elId);
	    var hide = document.getElementById('toggle-site');
	    hide.classList.add('hide');
	    model.site = input.value;
	    input.value = '';
	    model.bind(model, 'body');
	    return model;
	}

	function saveAboutPhone(elId, model) {
	    var input = document.getElementById(elId);
	    var hide = document.getElementById('toggle-phone');
	    hide.classList.add('hide');
	    model.phone = input.value;
	    input.value = '';
	    model.bind(model, 'body');
	    return model;
	}

	function saveAboutLocation(elId, model) {
	    var input = document.getElementById(elId);
	    var hide = document.getElementById('toggle-location');
	    hide.classList.add('hide');
	    model.location = input.value;
	    input.value = '';
	    model.bind(model, '.body');
	    return model;
	}

	function cancel(elId) {
	    var input = document.getElementById(elId);

	    var hide = document.getElementById('toggle-name');
	    hide.classList.add('hide');

	    hide = document.getElementById('toggle-site');
	    hide.classList.add('hide');

	    hide = document.getElementById('toggle-phone');
	    hide.classList.add('hide');

	    hide = document.getElementById('toggle-location');
	    hide.classList.add('hide');

	    input.value = '';
	}

	function openEditName(elId) {
	    var input = document.getElementById(elId);
	    var hide = document.getElementById('toggle-name');
	    hide.classList.remove('hide');
	}

	function openEditSite(elId) {
	    var input = document.getElementById(elId);
	    var hide = document.getElementById('toggle-site');
	    hide.classList.remove('hide');
	}

	function openEditPhone(elId) {
	    var input = document.getElementById(elId);
	    var hide = document.getElementById('toggle-phone');
	    hide.classList.remove('hide');
	}

	function openEditLocation(elId) {
	    var input = document.getElementById(elId);
	    var hide = document.getElementById('toggle-location');
	    hide.classList.remove('hide');
	}

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.AboutModel = undefined;

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _binder = __webpack_require__(9);

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var AboutModel = exports.AboutModel = function () {
	    function AboutModel(title, name, site, phone, location) {
	        _classCallCheck(this, AboutModel);

	        this.title = title;
	        this.name = name;
	        this.site = site;
	        this.phone = phone;
	        this.location = location;

	        //this.bind = Binder().bind(this, document.querySelector(".about"));
	    }

	    _createClass(AboutModel, [{
	        key: 'bind',
	        value: function bind(data, selector) {
	            (0, _binder.Binder)().bind(data, document.querySelector(selector));
	        }
	    }]);

	    return AboutModel;
	}();

/***/ }),
/* 9 */
/***/ (function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.Binder = Binder;
	function Binder() {
	    /**
	     * follows a path on the given data to retrieve a value
	     *
	     * @example
	     * var data = { foo : { bar : "abc" } };
	     * followPath(data, "foo.bar"); // "abc"
	     * 
	     * @param  {Object} data the object to get a value from
	     * @param  {String} path a path to a value on the data object
	     * @return the value of following the path on the data object
	     */
	    var followPath = function followPath(data, path) {
	        return path.split(".").reduce(function (prev, curr) {
	            return prev && prev[curr];
	        }, data);
	    };

	    /**
	     * sets value of an element based on it's data-value attribute
	     * 
	     * @param  {Object}  data     the data source
	     * @param  {Element} element  the element
	     */
	    var bindSingleElement = function bindSingleElement(data, element) {
	        var path = element.getAttribute("data-value");
	        element.innerText = followPath(data, path);
	    };

	    /**
	     * Binds data object to an element. Allows arbitrary nesting of fields
	     *
	     * @example
	     * <div class="user">
	     *   <p data-value="name"></p>
	     * </div>
	     * 
	     * var element = document.querySelector(".user");
	     * bind({ name : "Nick" }, element);
	     * 
	     * @param  {Object}  data     the data to bind to an element
	     * @param  {Element} element  the element to bind data to
	     */
	    var bind = function bind(data, element) {
	        var holders = element.querySelectorAll("[data-value]");
	        [].forEach.call(holders, bindSingleElement.bind(null, data));
	    };

	    return {
	        followPath: followPath,
	        bindSingleElement: bindSingleElement,
	        bind: bind
	    };
	}

/***/ }),
/* 10 */
/***/ (function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var SettingsModel = exports.SettingsModel = function SettingsModel(title) {
	    _classCallCheck(this, SettingsModel);

	    this.title = title;
	};

/***/ }),
/* 11 */
/***/ (function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var Option1Model = exports.Option1Model = function Option1Model(title) {
	    _classCallCheck(this, Option1Model);

	    this.title = title;
	};

/***/ }),
/* 12 */
/***/ (function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var Option2Model = exports.Option2Model = function Option2Model(title) {
	    _classCallCheck(this, Option2Model);

	    this.title = title;
	};

/***/ }),
/* 13 */
/***/ (function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var Option3Model = exports.Option3Model = function Option3Model(title) {
	    _classCallCheck(this, Option3Model);

	    this.title = title;
	};

/***/ })
/******/ ]);