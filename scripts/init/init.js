import { about } from '../templates/about-template';
import { settings } from '../templates/settings-template';
import { option1 } from '../templates/option1-template';
import { option2 } from '../templates/option2-template';
import { option3 } from '../templates/option3-template';

import { saveAboutName, saveAboutSite, saveAboutPhone, saveAboutLocation, cancel, openEditName, openEditSite, openEditPhone, openEditLocation } from '../controllers/about-controller';

import { AboutModel } from '../models/about-model';
import { SettingsModel } from '../models/settings-model';
import { Option1Model } from '../models/option1-model';
import { Option2Model } from '../models/option2-model';
import { Option3Model } from '../models/option3-model';

var aboutModel = new AboutModel('About', 'Jessica Parker', 'www.seller.com', '(949) 325 - 68594', 'Newport Beach, CA');
var settingsModel = new SettingsModel('Settings');
var option1Model = new Option1Model('Option1');
var option2Model = new Option2Model('Option2');
var option3Model = new Option3Model('Option3');
const templateContentId = 'template-content';
export class Init {
    constructor() {
        this.aboutModel = aboutModel;
        this.settingsModel = settingsModel;
        this.option1Model = option1Model;
        this.option2Model = option2Model;
        this.option3Model = option3Model;

        this.templateList = this.tpls();
    }

    tpls() {
        return [
            about(this.aboutModel),
            settings(this.settingsModel),
            option1(this.option1Model),
            option2(this.option2Model),
            option3(this.option3Model)
        ]

    }

    updateTpls() {
        this.templateList = this.tpls();
    }

    getNabarActiveLink() {
        var elements = document.getElementsByClassName('page-link');

        for (var i = 0; i < elements.length; i++) {
            var element = elements[i];
            //console.log(element)
            if (element.parentElement.classList.contains('nav-ul__li--active')) {
                return element.getAttribute('href').substring(1);
            }

        }
        throw new Error('No active link');

    }

    loadTemplate() {
        var activeLink = this.getNabarActiveLink();
        for (var i = 0; i < this.templateList.length; i++) {
            var t = this.templateList[i];
            if (t.templateName == activeLink) {
                var element = document.getElementById(templateContentId);
                element.innerHTML = t.templateValue;
                this.aboutModel.bind(this.aboutModel, 'body')
                return;
            }

        }

        throw new Error('Link and template do not match');
    }

    activateLink(obj) {
        var elements = document.getElementsByClassName('nav-ul__li--active');
        if (elements.length == 0)
            throw new Error('No active link');
        var element = elements[0];
        element.classList.remove('nav-ul__li--active');
        obj.parentElement.classList.add('nav-ul__li--active');
        this.loadTemplate();
    }

    san(data) {
        this.aboutModel = saveAboutName(data, this.aboutModel);
        this.updateTpls();

    }

    sas(data) {
        this.aboutModel = saveAboutSite(data, this.aboutModel);
        this.updateTpls();
    }

    sap(data) {
        this.aboutModel = saveAboutPhone(data, this.aboutModel);
        this.updateTpls();
    }

    sal(data) {
        this.aboutModel = saveAboutLocation(data, this.aboutModel);
        this.updateTpls();
    }

    oen(data) {
        openEditName(data);
    }

    oes(data) {
        openEditSite(data);
    }

    oep(data) {
        openEditPhone(data);
    }

    oel(data) {
        openEditLocation(data);
    }

    cancel(data) {
        return cancel(data);
        this.updateTpls();
    }

}