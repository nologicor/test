export function saveAboutName(elId, model) {
    var input = document.getElementById(elId);
    var hide = document.getElementById('toggle-name');
    hide.classList.add('hide');
    model.name = input.value;
    input.value = '';
    model.bind(model, 'body');
    return model
}

export function saveAboutSite(elId, model) {
    var input = document.getElementById(elId);
    var hide = document.getElementById('toggle-site');
    hide.classList.add('hide');
    model.site = input.value;
    input.value = '';
    model.bind(model, 'body');
    return model
}

export function saveAboutPhone(elId, model) {
    var input = document.getElementById(elId);
    var hide = document.getElementById('toggle-phone');
    hide.classList.add('hide');
    model.phone = input.value;
    input.value = '';
    model.bind(model, 'body');
    return model
}

export function saveAboutLocation(elId, model) {
    var input = document.getElementById(elId);
    var hide = document.getElementById('toggle-location');
    hide.classList.add('hide');
    model.location = input.value;
    input.value = '';
    model.bind(model, '.body');
    return model
}

export function cancel(elId) {
    var input = document.getElementById(elId);

    var hide = document.getElementById('toggle-name');
    hide.classList.add('hide');

    hide = document.getElementById('toggle-site');
    hide.classList.add('hide');

    hide = document.getElementById('toggle-phone');
    hide.classList.add('hide');

    hide = document.getElementById('toggle-location');
    hide.classList.add('hide');

    input.value = '';
}

export function openEditName(elId) {
    var input = document.getElementById(elId);
    var hide = document.getElementById('toggle-name');
    hide.classList.remove('hide');
}

export function openEditSite(elId) {
    var input = document.getElementById(elId);
    var hide = document.getElementById('toggle-site');
    hide.classList.remove('hide');
}

export function openEditPhone(elId) {
    var input = document.getElementById(elId);
    var hide = document.getElementById('toggle-phone');
    hide.classList.remove('hide');
}

export function openEditLocation(elId) {
    var input = document.getElementById(elId);
    var hide = document.getElementById('toggle-location');
    hide.classList.remove('hide');
}