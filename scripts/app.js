import { Init } from './init/init';

window.onload = function() {
    var init = new Init();
    window.init = init;
    init.loadTemplate();
}