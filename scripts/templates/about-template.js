export function about(model = '') {
    const
        inputName = 'input-name',
        inputSite = 'input-site',
        inputPhone = 'input-phone',
        inputLocation = 'input-location';


    return {
        templateName: 'about',
        templateValue: `
			<h3>${model.title}</h3>
			<br>
			<div class="about">
				<div class="about__field about__field--first">
					<div class="about__field__info">
						<span class="about__field__info__data" data-value="name"></span>
						<span onclick="init.oen()" class="about__field__info__edit"><i class="ion-android-create"></i></span>
					</div>
					<div id="toggle-name" class="about__field__inputs hide">
						<div class="input-group">
							<input type="text" id="${inputName}" required>
							<span class="highlight"></span>
							<span class="bar"></span>
							<label>NAME</label>
						</div>
						<div class="class="about__field__inputs--button">
							<input class="button--full" type="submit" value="Submit" onclick="init.san('${inputName}'); return false;">
							<input class="button--empty" type="submit" value="Cancel" onclick="init.cancel('${inputName}'); return false;">
						</div>
					</div>
				</div>
				<br>
				<div class="about__field">
					<div class="about__field__info">
						<i class="ion-android-globe about__field__icon"></i>
						<span class="about__field__info__data" data-value="site">${model.site}</span>
						<span onclick="init.oes()" class="about__field__info__edit"><i class="ion-android-create about__field__edit"></i></span>
					</div>
					<div id="toggle-site" class="about__field__inputs hide">
						<div class="input-group">
							<input type="text" id="${inputSite}" required>
							<span class="highlight"></span>
							<span class="bar"></span>
							<label>WEBSITE</label>
						</div>
						<div class="about__field__inputs--button">
							<input class="button--full" type="submit" value="Submit" onclick="init.sas('${inputSite}'); return false;">
							<input class="button--empty" type="submit" value="Cancel" onclick="init.cancel('${inputSite}'); return false;">
						</div>
					</div>
					
				</div>
				<br>

				<div class="about__field">
					<div class="about__field__info">
						<i class="ion-ios-telephone-outline about__field__icon"></i>
						<span class="about__field__info__data" data-value="phone">${model.phone}</span>
						<span onclick="init.oep()" class="about__field__info__edit"><i class="ion-android-create about__field__edit"></i></span>
					</div>
					<div id="toggle-phone" class="about__field__inputs hide">
						<div class="input-group">
							<input type="text" id="${inputPhone}" required>
							<span class="highlight"></span>
							<span class="bar"></span>
							<label>Phone</label>
						</div>
						<div class="about__field__inputs--button">
							<input class="button--full" type="submit" value="Submit" onclick="init.sap('${inputPhone}'); return false;">
							<input class="button--empty" type="submit" value="Cancel" onclick="init.cancel('${inputPhone}'); return false;">
						</div>
					</div>
				</div>
				<br>
				<div class="about__field">
					<div class="about__field__info">
					<i class="ion-ios-home-outline about__field__icon"></i>
						<span class="about__field__info__data" data-value="location">${model.location}</span>
						<span onclick="init.oel()" class="about__field__info__edit"><i class="ion-android-create about__field__edit"></i></span>
					</div>
					<div id="toggle-location" class="about__field__inputs hide">
						<div class="input-group">
							<input type="text" id="${inputLocation}" required>
							<span class="highlight"></span>
							<span class="bar"></span>
							<label>CITY,STATE & ZIP</label>
						</div>
						<div class="about__field__inputs--button">
							<input class="button--full" type="submit" value="Submit" onclick="init.sal('${inputLocation}'); return false;">
							<input class="button--empty" type="submit" value="Cancel" onclick="init.cancel('${inputLocation}'); return false;">
						</div>
					</div>
				</div>
			</div>
			`
    };
}