import { Binder } from '../init/binder';

export class AboutModel {
    constructor(title, name, site, phone, location) {
        this.title = title;
        this.name = name;
        this.site = site;
        this.phone = phone;
        this.location = location;

        //this.bind = Binder().bind(this, document.querySelector(".about"));
    }


    bind(data, selector) {
        Binder().bind(data, document.querySelector(selector));
    }

}